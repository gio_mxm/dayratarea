/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moto;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author USUARIO
 */
public class Modelo extends AbstractTableModel {

    private ArrayList<Moto> moto;

    public Modelo() {

    }

    public Modelo(ArrayList<Moto> moto) {
        this.moto = moto;
    }

    @Override
    public int getRowCount() {
        return moto.size();
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    @Override
    public String getColumnName(int column) {

        String titco = "";
        switch (column) {
            case 0:
                titco = "Marca";
                break;
            case 1:
                titco = "Color";
                break;
            case 2:
                titco = "Transmision";
                break;
            case 3:
                titco = "Modelo";
                break;
            case 4:
                titco = "Peso";
                break;
            case 5:
                titco = "Longitud";
                break;
            case 6:
                titco = "Altura";
                break;
            case 7:
                titco = "Tipo";
                break;
            default:
                titco = "";
        }

        return titco;

    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Moto gv = moto.get(rowIndex);

        switch (columnIndex) {
            case 0:

                return gv.getMarca();
            case 1:

                return gv.getColor();
            case 2:

                return gv.getTrans();
            case 3:

                return gv.getModelo();
            case 4:

                return gv.getPeso();
            case 5:

                return gv.getLongitud();
            case 6:

                return gv.getAltura();
            case 7:

                return gv.getTipo();

            default:
                return null;
        }

    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Moto gv = moto.get(rowIndex);
        switch (columnIndex) {
            case 0:
                gv.setMarca((String) aValue);
                break;
            case 1:
                gv.setColor((String) aValue);
                break;
            case 2:
                gv.setTrans((String) aValue);
                break;
            case 3:
                gv.setModelo((String) aValue);
                break;
            case 4:
                gv.setPeso(((Integer) aValue));
                break;
            case 5:
                gv.setLongitud((Integer) aValue);
                break;
            case 6:
                gv.setAltura((Integer) aValue);
                break;
            case 7:
                gv.setTipo((String) aValue);
                break;

            default:
                throw new AssertionError();
        }
        moto.set(rowIndex, gv);
        this.fireTableCellUpdated(rowIndex, columnIndex);

    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:

                return String.class;
            case 1:

                return String.class;
            case 2:

                return String.class;
            case 3:

                return String.class;
            case 4:

                return Float.class;
            case 5:

                return Float.class;
            case 6:

                return Float.class;
            case 7:

                return String.class;

            default:
                return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public void moton (Moto nmoto){
    
        moto.add(nmoto);
        this.fireTableDataChanged();
        
    }
    
    
}
