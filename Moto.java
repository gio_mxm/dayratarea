/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moto;

/**
 *
 * @author USUARIO
 */
public class Moto {
    private String marca;
    private String color;
    private String trans;
    private String modelo;
    private float peso;
    private float longitud;
    private float altura;
    private String tipo;

    public Moto() {
    }

    public Moto(String marca, String color, String trans, String modelo, float peso, float longitud, float altura, String tipo) {
        this.marca = marca;
        this.color = color;
        this.trans = trans;
        this.modelo = modelo;
        this.peso = peso;
        this.longitud = longitud;
        this.altura = altura;
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTrans() {
        return trans;
    }

    public void setTrans(String trans) {
        this.trans = trans;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public float getLongitud() {
        return longitud;
    }

    public void setLongitud(float longitud) {
        this.longitud = longitud;
    }

    public float getAltura() {
        return altura;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

    @Override
    public String toString() {
        return "Moto:" + "Marca: " + marca + "Color: " + color + "Transmisicon: "
                + trans + "Modelo: " + modelo + "Peso: "+ peso + "Longitud: " 
                + longitud + "Altura: " + altura + "Tipo: " + tipo;
                
    }

    
    
    
    
    
    
}
